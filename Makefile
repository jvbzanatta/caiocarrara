#Copyright (C) 2019  Caio Carrara <eu@caiocarrara.com.br>
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <https://www.gnu.org/licenses/>.
default: test

run:
	python hourglass/manage.py runserver

run-migrate:
	python hourglass/manage.py migrate

make-migrations:
	python hourglass/manage.py makemigrations

shell:
	python hourglass/manage.py shell

clean: clean-eggs clean-build
	@find . -iname '*.pyc' -delete
	@find . -iname '*.pyo' -delete
	@find . -iname '*~' -delete
	@find . -iname '*.swp' -delete
	@find . -iname '__pycache__' -delete

clean-eggs:
	@find . -name '*.egg' -print0|xargs -0 rm -rf --
	@rm -rf .eggs/

clean-build:
	@rm -fr build/
	@rm -fr dist/
	@rm -fr *.egg-info

test: clean
	pytest hourglass

test-matching: clean
	pytest hourglass -sk $(filter-out $@, $(MAKECMDGOALS))

test-coverage:
	pytest hourglass --cov-report=html

install-production:
	pip install -r requirements/production.txt

install-local:
	pip install -r requirements/local.txt

install-test:
	pip install -r requirements/test.txt

${VIRTUAL_ENV}/bin/pip-sync:
	pip install pip-tools

pip-tools: ${VIRTUAL_ENV}/bin/pip-sync

pip-compile: pip-tools
	@rm -f requirements/production.txt
	pip-compile requirements/production.in

pip-install: pip-compile
	pip install --upgrade -r requirements/local.txt

pip-upgrade: pip-tools
	pip-compile --upgrade requirements/production.in
