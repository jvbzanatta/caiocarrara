# Hourglass - Time Tracking API


## Project time tracked
The hours spent followed by each task can be found on the following
link:

* Hours: https://s.caiocarrara.com.br/vibbra-horas.pdf

## Test Environment
The project is currently deployed in a test environment accessible by
the following URL:

http://vibbra-hourglass-api.herokuapp.com/v1/

The API docs are deployed in two different formats:

* Swagger: http://vibbra-hourglass-api.herokuapp.com/v1/swagger/
* ReDoc: http://vibbra-hourglass-api.herokuapp.com/v1/redoc/

The following credentials can be used on test environment:
```
{
  "login": "apiuser",
  "password": "testuser2019"
}
```

## System requirements
* Python 3.7
* PostgreSQL Database (for production)


## Installing
**Important:** It's recommended to use a solution to isolate the Python
dependencies environment like pipenv or virtualenv. It's suggested the
use of the virtualenv option. It can be used in a bunch of different ways.
Check this [virtualenv
documentation](https://docs.python-guide.org/dev/virtualenvs/#lower-level-virtualenv).

With the `virtualenv` activated to install the project dependencies it's
just a matter of running the following command for a local installation:
```
$ make install-local
```


## Configuration
All configuration constants expected to run the project can be found in
the `env.sample` file. The constants in this file can be copied in a
**development environment** to a other file named `.env` so they can be
loaded by the project. In **production environment** these constants
**must** be set as environment variables. [Read more about 12Factor
config](https://12factor.net/config).

### Database
One of the configuration constant set in `.env` file or through
environment variables is the `DATABASE_URL`. The database url must
follow the format according the database being used. The url examples
can be checked in this [URL Schema
doc](https://github.com/kennethreitz/dj-database-url#url-schema).

If no `DATABASE_URL` be specified a local sqlite database will be
created on first migration.


## Utility commands ready to use
The project comes with a set of commands (make target actions) ready to
use. The main commands are documented below, but check the `Makefile` on
project root to see all commands available.


1. Installing dependencies locally
```
$ make install-local
```

2. Installing dependencies in production
```
$ make install-production
```

3. Updating dependencies versions on requirements file
```
$ make pip-upgrade
```

4. Running the project migrations (create/upgrade database schema)
```
$ make run-migrate
```

5. Running the project locally (available on port 8000)
```
$ make run
```
