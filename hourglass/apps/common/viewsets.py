"""
Copyright (C) 2019  Caio Carrara <eu@caiocarrara.com.br>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from rest_framework import status, viewsets
from rest_framework.response import Response


class BaseModelViewSet(viewsets.ModelViewSet):
    write_serializer_class = None

    def create(self, request, *args, **kwargs):
        if not self.write_serializer_class:
            return super().create(request, *args, **kwargs)

        write_serializer = self.write_serializer_class(data=request.data)
        write_serializer.is_valid(raise_exception=True)
        self.perform_create(write_serializer)

        read_serializer = self.get_serializer_class()(write_serializer.instance)
        headers = self.get_success_headers(read_serializer.data)

        return Response(
            read_serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )

    def update(self, request, *args, **kwargs):
        if not self.write_serializer_class:
            return super().update(request, *args, **kwargs)

        partial = kwargs.pop("partial", False)
        instance = self.get_object()
        write_serializer = self.write_serializer_class(
            instance, data=request.data, partial=partial
        )
        write_serializer.is_valid(raise_exception=True)
        self.perform_update(write_serializer)

        read_serializer = self.get_serializer_class()(instance)

        return Response(read_serializer.data)
