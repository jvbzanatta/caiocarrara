"""
Copyright (C) 2019  Caio Carrara <eu@caiocarrara.com.br>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import ugettext_lazy as _

from apps.common.models import BaseModel, SlugModelMixin

User = get_user_model()


class Project(SlugModelMixin, BaseModel):
    field_to_slug = 'title'

    title = models.CharField(_('Title'), max_length=128, unique=True)
    description = models.TextField(_('Description'), blank=True)

    users = models.ManyToManyField(User, verbose_name=_('Users'), related_name='projects')

    class Meta:
        ordering = ['title']
        verbose_name = _('Project')
        verbose_name_plural = _('Projects')

    def __str__(self):
        return f'{self.title} ({self.pk})'


class Time(BaseModel):
    project = models.ForeignKey(
        Project,
        verbose_name=_('Project'),
        on_delete=models.CASCADE,
        related_name='times',
    )
    started_at = models.DateTimeField(blank=True, null=True)
    ended_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ['-started_at']
        verbose_name = _('Time')
        verbose_name_plural = _('Times')

    def __str__(self):
        started_at = self.started_at.isoformat() if self.started_at else _('not informed')
        ended_at = self.ended_at.isoformat() if self.ended_at else _('not informed')
        return f'{started_at, ended_at} ({self.project})'
