"""
Copyright (C) 2019  Caio Carrara <eu@caiocarrara.com.br>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from django.contrib.auth import get_user_model
from rest_framework import serializers

from apps.common.viewsets import BaseModelViewSet
from apps.realm.views import UserSerializer
from .models import Project, Time

User = get_user_model()


class ProjectSerializer(serializers.ModelSerializer):
    users = UserSerializer(many=True)

    class Meta:
        model = Project
        fields = ('id', 'title', 'description', 'slug', 'users')


class ProjectWriteSerializer(serializers.ModelSerializer):
    users = serializers.PrimaryKeyRelatedField(
        allow_empty=True,
        many=True,
        queryset=User.objects.all()
    )

    class Meta:
        model = Project
        fields = ('id', 'title', 'description', 'slug', 'users')


class TimeSerializer(serializers.ModelSerializer):
    project = ProjectSerializer()

    class Meta:
        model = Time
        fields = ('id', 'project', 'started_at', 'ended_at')


class TimeWriteSerializer(serializers.ModelSerializer):
    project = serializers.PrimaryKeyRelatedField(queryset=Project.objects.all())

    class Meta:
        model = Time
        fields = ('id', 'project', 'started_at', 'ended_at')

    @property
    def is_update(self):
        return self.instance is not None

    def is_full_update(self, started_at, ended_at):
        return started_at and ended_at

    def validate(self, data):
        started_at = data.get('started_at')
        ended_at = data.get('ended_at')

        if self.is_full_update(started_at, ended_at):
            self._validate_times(started_at, ended_at)
            return data

        if self.is_update:
            self._validate_update(started_at, ended_at)
            return data

        return data

    def _validate_update(self, started_at, ended_at):
        current_started_at = self.instance.started_at
        current_ended_at = self.instance.ended_at

        if started_at:
            return self._validate_times(started_at, current_ended_at)

        if ended_at:
            return self._validate_times(current_started_at, ended_at)

    def _validate_times(self, started_at, ended_at):
        if started_at > ended_at:
            raise serializers.ValidationError('Your time must start before its end')


class ProjectViewSet(BaseModelViewSet):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    write_serializer_class = ProjectWriteSerializer


class TimeViewSet(BaseModelViewSet):
    queryset = Time.objects.all()
    serializer_class = TimeSerializer
    write_serializer_class = TimeWriteSerializer
