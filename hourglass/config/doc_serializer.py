from rest_framework import serializers


class AuthSerializer(serializers.Serializer):
    token = serializers.CharField()
