"""
Copyright (C) 2019  Caio Carrara <eu@caiocarrara.com.br>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from django.contrib import admin
from django.urls import include, path, re_path
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from drf_yasg.views import get_schema_view
from rest_framework import permissions, status
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token

from .doc_serializer import AuthSerializer


schema_view = get_schema_view(
    openapi.Info(
        title="Hourglass - Time Tracking API",
        default_version='v1',
        description="This is a time tracking API",
        contact=openapi.Contact(email="caio@twelvestudio.tech"),
        license=openapi.License(name="GPLv3"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)


auth_view = swagger_auto_schema(
    method='post',
    responses={status.HTTP_200_OK: AuthSerializer}
)(obtain_jwt_token)


urlpatterns = [
    path('admin/', admin.site.urls),

    path('v1/authenticate', auth_view, name='auth'),
    path('v1/refresh-token', refresh_jwt_token, name='refresh-token'),

    re_path(
        r'^v1/swagger(?P<format>\.json|\.yaml)$',
        schema_view.without_ui(cache_timeout=0),
        name='schema-json'
    ),
    re_path(
        r'^v1/swagger/$',
        schema_view.with_ui('swagger', cache_timeout=0),
        name='schema-swagger-ui'
    ),
    re_path(
        r'^v1/redoc/$',
        schema_view.with_ui('redoc', cache_timeout=0),
        name='schema-redoc'
    ),

    path('v1/', include('apps.realm.urls', namespace='realm')),
    path('v1/', include('apps.projects.urls', namespace='projects')),
]
