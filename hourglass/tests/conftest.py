"""
Copyright (C) 2019  Caio Carrara <eu@caiocarrara.com.br>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import pytest
from django.contrib.auth import get_user_model
from django.db import IntegrityError
from django.urls import reverse

User = get_user_model()


@pytest.fixture
def api_user(django_db_blocker):
    with django_db_blocker.unblock():
        try:
            user = User.objects.create_user(login='apiclient', password='123test123')
        except IntegrityError:
            user = User.objects.get(login='apiclient')
    return user


@pytest.fixture
def auth_token(client, api_user):
    payload = {'login': 'apiclient', 'password': '123test123'}
    response = client.post(
        reverse('auth'),
        data=payload,
        content_type='application/json'
    )
    return response.json()['token']


@pytest.fixture
def auth_header(auth_token):
    return {'HTTP_AUTHORIZATION': f'JWT {auth_token}'}
