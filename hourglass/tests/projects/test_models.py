"""
Copyright (C) 2019  Caio Carrara <eu@caiocarrara.com.br>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import pytest

from django.db import IntegrityError
from django.utils import timezone
from mixer.backend.django import mixer

from apps.projects.models import Project, Time

pytestmark = pytest.mark.django_db


def test_project_title_slugify():
    project = mixer.blend(Project, title='This is My Title')
    assert project.slug == 'this-is-my-title'


def test_project_title_unique():
    mixer.blend(Project, title='unique title')

    with pytest.raises(IntegrityError):
        mixer.blend(Project, title='unique title')


def test_project_as_string():
    project = mixer.blend(Project, title='My Name')
    assert str(project) == f'{project.title} ({project.pk})'


def test_project_default_ordering():
    mixer.cycle(2).blend(Project, title=(title for title in ('Foo', 'Bar')))
    assert 'Bar' == Project.objects.all()[0].title
    assert 'Foo' == Project.objects.all()[1].title


def test_time_as_str():
    yesterday = timezone.now() - timezone.timedelta(days=1)
    _time = mixer.blend(Time, started_at=yesterday, ended_at=timezone.now())
    assert str(_time) == f'{_time.started_at.isoformat(), _time.ended_at.isoformat()} ({_time.project})'


def test_time_default_ordering():
    early_hour = timezone.now() - timezone.timedelta(hours=5)
    late_hour = timezone.now() - timezone.timedelta(hours=1)

    mixer.blend(Time, started_at=early_hour, ended_at=timezone.now())
    late_time = mixer.blend(Time, started_at=late_hour, ended_at=timezone.now())

    assert Time.objects.all()[0].pk == late_time.pk
