"""
Copyright (C) 2019  Caio Carrara <eu@caiocarrara.com.br>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import json
import random

import pytest
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.utils import timezone
from mixer.backend.django import mixer
from rest_framework import status

from apps.projects.models import Project, Time

pytestmark = pytest.mark.django_db
User = get_user_model()


@pytest.fixture
def users():
    return mixer.cycle(3).blend(User)


@pytest.fixture
def project(users):
    project = mixer.blend(Project)
    project.users.add(random.choice(users))
    project.save()
    return project


@pytest.fixture
def project_payload(users):
    return {
        'users': [str(user.pk) for user in users],
        'title': 'Testing Project',
        'description': 'This is the Testing Project description',
    }


@pytest.fixture
def time_payload(project):
    return {
        'project': str(project.pk),
        'started_at': '2019-05-24T21:00:00Z',
        'ended_at': '2019-05-24T22:00:00Z',
    }


@pytest.fixture
def time():
    yesterday = timezone.now() - timezone.timedelta(days=1)
    return mixer.blend(Time, started_at=yesterday, ended_at=timezone.now())


def test_get_projects_successfully(client, auth_header):
    mixer.cycle(5).blend(Project)
    projects_url = reverse('projects:project-list')
    response = client.get(projects_url, **auth_header)

    assert response.status_code == status.HTTP_200_OK
    assert response.json()['count'] == 5


def test_get_specific_project(client, project, auth_header):
    response = client.get(
        reverse('projects:project-detail', kwargs={'pk': project.pk}), **auth_header
    )
    assert response.status_code == status.HTTP_200_OK
    assert response.json()['id'] == str(project.pk)


def test_create_project_successfully(client, project_payload, auth_header):
    response = client.post(
        reverse('projects:project-list'),
        json.dumps(project_payload),
        content_type='application/json',
        **auth_header
    )
    assert response.status_code == status.HTTP_201_CREATED
    assert response.json()['title'] == project_payload['title']


@pytest.mark.parametrize('http_method', ('patch', 'put'))
def test_update_project_users_successfully(client, http_method, project_payload, auth_header):
    client_request = getattr(client, http_method)
    project = mixer.blend(Project, title='Foo', description='Bar')

    response = client_request(
        reverse('projects:project-detail', kwargs={'pk': project.pk}),
        json.dumps(project_payload),
        content_type='application/json',
        **auth_header,
    )

    assert response.status_code == status.HTTP_200_OK
    assert response.json()['title'] == project_payload['title']
    assert response.json()['description'] == project_payload['description']


def test_update_project_partially(client, auth_header):
    project = mixer.blend(Project, title='Foo', description='Bar')
    expected_description = 'This is a updated description'
    update_payload = {'description': expected_description}

    response = client.patch(
        reverse('projects:project-detail', kwargs={'pk': project.pk}),
        json.dumps(update_payload),
        content_type='application/json',
        **auth_header,
    )

    assert response.status_code == status.HTTP_200_OK
    assert response.json()['description'] == expected_description


def test_get_times_successfylly(client, auth_header):
    mixer.cycle(5).blend(Time)
    time_url = reverse('projects:time-list')
    response = client.get(time_url, **auth_header)

    assert response.status_code == status.HTTP_200_OK
    assert response.json()['count'] == 5


def test_get_specific_time(client, time, auth_header):
    response = client.get(
        reverse('projects:time-detail', kwargs={'pk': time.pk}), **auth_header
    )
    assert response.status_code == status.HTTP_200_OK
    assert response.json()['id'] == str(time.pk)


def test_create_time_successfully(client, time_payload, auth_header):
    response = client.post(
        reverse('projects:time-list'),
        json.dumps(time_payload),
        content_type='application/json',
        **auth_header,
    )
    assert response.status_code == status.HTTP_201_CREATED


@pytest.mark.parametrize('http_method', ('patch', 'put'))
def test_update_time_successfully(client, http_method, time_payload, auth_header):
    client_request = getattr(client, http_method)
    first_time = timezone.now() - timezone.timedelta(days=10)
    second_time = timezone.now() - timezone.timedelta(days=5)
    time = mixer.blend(Time, started_at=first_time, ended_at=second_time)

    response = client_request(
        reverse('projects:time-detail', kwargs={'pk': time.pk}),
        json.dumps(time_payload),
        content_type='application/json',
        **auth_header,
    )

    assert response.status_code == status.HTTP_200_OK
    assert response.json()['started_at'] == time_payload['started_at']
    assert response.json()['ended_at'] == time_payload['ended_at']


@pytest.mark.parametrize('attribute_to_omit', ('started_at', 'ended_at'))
def test_create_time_partially(client, attribute_to_omit, time_payload, auth_header):
    del(time_payload[attribute_to_omit])

    response = client.post(
        reverse('projects:time-list'),
        json.dumps(time_payload),
        content_type='application/json',
        **auth_header,
    )

    assert response.status_code == status.HTTP_201_CREATED
    assert not response.json()[attribute_to_omit]


def test_validate_started_after_ended_at(client, time_payload, auth_header):
    time_payload['started_at'], time_payload['ended_at'] = time_payload['ended_at'], time_payload['started_at']

    response = client.post(
        reverse('projects:time-list'),
        json.dumps(time_payload),
        content_type='application/json',
        **auth_header,
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert 'Your time must start before its end' in response.json()['non_field_errors']
