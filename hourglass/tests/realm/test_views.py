"""
Copyright (C) 2019  Caio Carrara <eu@caiocarrara.com.br>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import json

import pytest
from django.urls import reverse
from django.contrib.auth import get_user_model
from mixer.backend.django import mixer
from rest_framework import status

pytestmark = pytest.mark.django_db
User = get_user_model()


@pytest.fixture
def user_payload():
    return {
        'login': 'iamtestuser',
        'password': '1029384756',
    }


def test_get_users_successfully(client, auth_header):
    mixer.cycle(5).blend(User)
    users_url = reverse('realm:user-list')
    response = client.get(users_url, **auth_header)

    assert response.status_code == status.HTTP_200_OK
    assert response.json()['count'] == 6


def test_get_specific_user(client, auth_header):
    user = mixer.blend(User)
    response = client.get(
        reverse('realm:user-detail', kwargs={'pk': user.pk}),
        **auth_header
    )
    assert response.status_code == status.HTTP_200_OK
    assert response.json()['login'] == user.login


def test_create_user_successfully(client, user_payload, auth_header):
    response = client.post(
        reverse('realm:user-list'),
        json.dumps(user_payload),
        content_type='application/json',
        **auth_header,
    )

    del(user_payload['password'])
    assert response.status_code == status.HTTP_201_CREATED
    for attr, value in user_payload.items():
        assert response.json()[attr] == value


@pytest.mark.parametrize('http_method', ('patch', 'put'))
def test_update_user_successfully(client, http_method, user_payload, auth_header):
    del(user_payload['password'])
    client_request = getattr(client, http_method)
    user = mixer.blend(User, login='iamatestuser')

    response = client_request(
        reverse('realm:user-detail', kwargs={'pk': user.pk}),
        json.dumps(user_payload),
        content_type='application/json',
        **auth_header,
    )

    assert response.status_code == status.HTTP_200_OK
    assert response.json()['login'] == user_payload['login']
